<?php

namespace Blok\Graphql\Facades;

use Illuminate\Support\Facades\Facade;

class Graphql extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'graphql';
    }
}
