<?php

namespace Blok\Graphql;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    const CONFIG_PATH = __DIR__ . '/../config/graphql.php';

    public function boot()
    {
        $this->publishes([
            self::CONFIG_PATH => config_path('graphql.php'),
        ], 'config');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            self::CONFIG_PATH,
            'graphql'
        );

        $this->app->bind('graphql', function () {
            return new Graphql(config('graphql.url'), config('graphql.token'));
        });
    }
}
