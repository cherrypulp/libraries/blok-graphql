<?php

namespace Blok\Graphql;

class Graphql {

    public $endpoint;
    public $token;

    public function __construct($endpoint = null, $token = null)
    {
        $this->endpoint = $endpoint;
        $this->token = $token;
    }

    /**
     * Very simple query helper
     *
     * @param string $endpoint
     * @param string $query
     * @param array $variables
     * @param string|null $token
     * @return array
     * @throws \ErrorException
     */
    public function query(string $query, array $variables = [])
    {
        $endpoint = $this->endpoint;
        $token = $this->token;
        $headers = [
            'Content-Type: application/json',
            'User-Agent: GraphQL client',
        ];

        if (null !== $token) {
            $headers[] = "Authorization: Bearer $token";
        }

        $content = ['query' => $query, 'variables' => $variables];

        $params = [
            'http' => [
                'method' => 'POST',
                'header' => $headers,
                'content' => json_encode($content),
            ],
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
            ],
        ];

        $context = stream_context_create($params);

        try {
            $response = @file_get_contents($endpoint, false, $context);
        } catch (\ErrorException $exception) {
            throw new \ErrorException($exception->getMessage(), $exception->getCode(), $exception->getPrevious());
        } catch (Exception $exception){
            throw $exception;
        }

        if (!$response) {
            $error = error_get_last();

            if ($error) {
                throw new \ErrorException($error['message'], $error['type']);
            }

            \Log::error("Unknown graphql error", $params);

            throw new \Exception('Unknown GraphQl error '.json_encode($params));
        }

        $response = json_decode($response, true);

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new \ErrorException('Failed to parse JSON');
        }

        return $response;
    }
}
