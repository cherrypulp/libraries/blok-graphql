<?php

return [
    'url' => env('GRAPHQL_URL'),
    'token' => env('GRAPHQL_TOKEN'),
];
