# Graphql

[![Build Status](https://travis-ci.org/blok/graphql.svg?branch=master)](https://travis-ci.org/blok/graphql)
[![Packagist](https://img.shields.io/packagist/v/blok/graphql.svg)](https://packagist.org/packages/blok/graphql)
[![Packagist](https://poser.pugx.org/blok/graphql/d/total.svg)](https://packagist.org/packages/blok/graphql)
[![Packagist](https://img.shields.io/packagist/l/blok/graphql.svg)](https://packagist.org/packages/blok/graphql)

Package description :

Very simple GraphqlClient helper to make a graphql call.

## Installation

Install via composer
```bash
composer require blok/graphql
```

### Register Service Provider

**Note! This and next step are optional if you use laravel>=5.5 with package
auto discovery feature.**

Add service provider to `config/app.php` in `providers` section
```php
Blok\Graphql\ServiceProvider::class,
```

### Register Facade

Register package facade in `config/app.php` in `aliases` section
```php
Blok\Graphql\Facades\Graphql::class,
```

### Publish Configuration File

```bash
php artisan vendor:publish --provider="Blok\Graphql\ServiceProvider" --tag="config"
```

## Usage as a standalone php

Simply add your query and param like that (here is an exemple with the Opencollective GraphQL api) :

````php

use Blok\Graphql\Graphql;

    $query = <<<EOT
query allTransactions(\$slug: String){
  allTransactions(collectiveSlug: \$slug){
    id,
    uuid,
    amount,
    currency,
    description,
    hostCurrency,
    hostCurrencyFxRate,
    netAmountInCollectiveCurrency,
    type,
    createdAt,
    updatedAt,
    refundTransaction{id},
    collective{id, name, slug},
  }
}
EOT;

    $graphql = new Graphql(YOUR_URL, YOUR_BEARER_TOKEN);
    $result = $graphql->query($query, ['slug' => 'co-labs']);

````

# Use as a Laravel Graphql instance

For convenience you have also an instanciated graphql service provider that will use the setup from your env file : GRAPHQL_URL and GRAPHQL_TOKEN.

To use it you can use the Facade or app :


````php

use Blok\Graphql\Facades\Graphql;

    $query = <<<EOT
query allTransactions(\$slug: String){
  allTransactions(collectiveSlug: \$slug){
    id,
    uuid,
    amount,
    currency,
    description,
    hostCurrency,
    hostCurrencyFxRate,
    netAmountInCollectiveCurrency,
    type,
    createdAt,
    updatedAt,
    refundTransaction{id},
    collective{id, name, slug},
  }
}
EOT;

    $result = app('graphql')->query($query, ['slug' => 'co-labs']);

    or

    $result = Graphql::query($query, ['slug' => 'co-labs']);

````

## Security

If you discover any security related issues, please email
instead of using the issue tracker.

## Credits

- [Cherry pulp](https://github.com/blok/graphql)
- [All contributors](https://github.com/blok/graphql/graphs/contributors)

This package is bootstrapped with the help of
[blok/laravel-package-generator](https://github.com/blok/laravel-package-generator).
