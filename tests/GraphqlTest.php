<?php

namespace Blok\Graphql\Tests;

use Blok\Graphql\Facades\Graphql;
use Blok\Graphql\ServiceProvider;
use Orchestra\Testbench\TestCase;

class GraphqlTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'graphql' => Graphql::class,
        ];
    }

    public function testExample()
    {
        $this->assertEquals(1, 1);
    }
}
